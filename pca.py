#!/usr/bin/env python2.7
# Charlie Johnston, Nick Rocco
# pca.py
# For Wedbush Securities

import os
import sys
import string
import numpy as np

# Global variables
COMMODITY = ''
FACTOR1 = ''
FACTOR2 = ''
FACTOR3 = ''
FACTOR4 = ''

# Function for usage
def usage(exit_code=0):
	print '''Usage: {} [-c COMMODITY -f1 FACTOR1 -f2 FACTOR2 -f3 FACTOR3 -f4 FACTOR4]
	-c COMMODITY	Commodity to be looked at
	-f1 FACTOR1	First factor for analysis
	-f2 FACTOR2	Second factor for analysis
	-f3 FACTOR3	Third factor for analysis
	-f4 FACTOR4	Fourth factor for analysis'''.format(os.path.basename(sys.argv[0]))
	sys,exit(exit_code)

# Function for subtracting the mean of each row from each data point in that row
def subtract_mean(list):
	sum = 0
	count = 0
	for i in list:
		sum = sum + float(i)
		count = count + 1
	
	average = sum/count
	
	index = 0
	for i in list:
		list[index] = float(list[index]) - average
		index = index + 1
	
	return list

# Reads in each text file and places the data into arrays
if __name__ == '__main__':
	args = sys.argv[1:]

	while len(args) and args[0].startswith('-') and len(args[0]) > 1:
		arg = args.pop(0)
		if arg == '-c':
			COMMODITY = args.pop(0)
		elif arg == '-f1':
			FACTOR1 = args.pop(0)
		elif arg == '-f2':
			FACTOR2 = args.pop(0)
		elif arg == '-f3':
			FACTOR3 = args.pop(0)
		elif arg == '-f4':
			FACTOR4 = args.pop(0)
		elif arg == '-h':
			usage(0)
		else:
			usage(1)

# Uses the subtract mean function on each array
	if COMMODITY != '':
		commodity = [i.rstrip() for i in open(COMMODITY)]
		commodity = subtract_mean(commodity) 
	if FACTOR1 != '':
		factor1 = [i.rstrip() for i in open(FACTOR1)]
		factor1 = subtract_mean(factor1)
	if FACTOR2 != '':
		factor2 = [i.rstrip() for i in open(FACTOR2)]
		factor2 = subtract_mean(factor2)
	if FACTOR3 != '':
		factor3 = [i.rstrip() for i in open(FACTOR3)]
		factor3 = subtract_mean(factor3)
	if FACTOR4 != '':
		factor4 = [i.rstrip() for i in open(FACTOR4)]
		factor4 = subtract_mean(factor4)

# If no file is given for factors 2-4, this will set their arrays as zero
	if FACTOR4 == '':
		factor4 = [0 for i in commodity]
	
	# Calculates the covariance matrix
	cov_mat = np.cov([commodity, factor1, factor2, factor3, factor4])
	
	# Calculates the eigenvalues and eigenvectors from the covariance matrix
	eig_val, eig_vec = np.linalg.eig(cov_mat)

	# Prints the eigenvalues and eigenvectors
	print (80 * '-')
	for i in range(len(eig_val)):
		if i == 0:
			continue
		else:
			eigvec = eig_vec[:,i].reshape(1,5).T
			print 'Eigenvector for factor {}: \n{}'.format(i, eigvec)
			print 'Eigenvalue for factor {}: \n{}'.format(i, eig_val[i])
			print (80 * '-')

	# Initializes the sum of the eigen values
	sum_eigen = 0
	
	# Sums up the eigen values for the different factors
	sum_eigen = float(eig_val[1]) + float(eig_val[2]) + float(eig_val[3]) + float(eig_val[4])

	# Calculates the percent in variation of the price and outputs whether or not the factor is a principal component
        for i in range(len(eig_val)):
                if i > 0:
                        value = eig_val[i]
                        percent = (value/sum_eigen) * 100
                        print 'Factor {} explains {}% of the variation in price.'.format(i, percent)
                        if percent >= 10:
                                print 'We suggest using Factor {} in your regression.'.format(i)
        print (80 * '-')

	# Make a list of (eigenvalue, eigenvector) tuples
	eig_pairs = [(np.abs(eig_val[i]), eig_vec[:,i]) for i in range(len(eig_val))]

	# Sort the (eigenvalue, eigenvector) tuples from high to low
	eig_pairs.sort(key = lambda x: x[0], reverse=True)
	
	# Choosing eigenvectros with largest eigenvalues
	matrix_w = np.hstack((eig_pairs[1][1].reshape(5,1), eig_pairs[2][1].reshape(5,1)))
	
	# Transforming the samples onto the new subspace
	transformed =  matrix_w.T.dot([commodity, factor1, factor2, factor3, factor4])

	# Print transformed matrix
	print 'Transformed matrix: '
	print transformed